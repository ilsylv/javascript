import { NavigationRoute } from './model';
import ListConstructors from '../containers/constructors/list';
import Circuits from '../components/circuits/circuits';

const routes: NavigationRoute[] = [
  {
    name: 'Constructors',
    path: '/constructors',
    Component: ListConstructors,
  },

  {
    name: 'Drivers',
    path: '/drivers',
    // Component: Pilot
  },

  {
    name: 'Circuits',
    path: '/circuits',
    Component: Circuits,
  },

  {
    name: 'Seasons',
    path: '/season',
    // Component: Team
  },

  {
    name: 'Main',
    path: '/javascript',
    // Component: Team
  },
];

export default routes;
