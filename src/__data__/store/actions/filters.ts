import {
  SET_CIRCUIT_COUNTRY,
  SET_CIRCUIT_NAME,
  SET_YEAR,
  SET_CONSTRUCTOR_NAME,
  SET_CONSTRUCTOR_COUNTRY,
  SET_DRIVER_SURNAME,
  SET_DRIVER_NATIONALITY,
} from '../action-types';
import { FiltersAction } from '../reducers/filters';

export const setFiltersYear = (year: number): FiltersAction => ({
  type: SET_YEAR,
  payload: { year },
});

export const setFiltersCircuitName = (circuitName: string): FiltersAction => ({
  type: SET_CIRCUIT_NAME,
  payload: { circuitName },
});

export const setFiltersCircuitCountry = (
  circuitCountry: string
): FiltersAction => ({
  type: SET_CIRCUIT_COUNTRY,
  payload: { circuitCountry },
});

export const setFiltersConstructorName = (
  constructorName: string
): FiltersAction => ({
  type: SET_CONSTRUCTOR_NAME,
  payload: { constructorName },
});

export const setFiltersConstructorCountry = (
  constructorCountry: string
): FiltersAction => ({
  type: SET_CONSTRUCTOR_COUNTRY,
  payload: { constructorCountry },
});

export const setFiltersDriverSurname = (
  driverSurname: string
): FiltersAction => ({
  type: SET_DRIVER_SURNAME,
  payload: { driverSurname },
});

export const setFiltersDriverNationality = (
  driverNationality: string
): FiltersAction => ({
  type: SET_DRIVER_NATIONALITY,
  payload: { driverNationality },
});
