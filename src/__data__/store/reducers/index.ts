import { combineReducers } from 'redux';
import filters, { FiltersState } from './filters';

export interface RootState {
  filters: FiltersState;
}

export default combineReducers<RootState>({
  filters,
});
