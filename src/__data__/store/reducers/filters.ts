import { Action } from 'redux';
import {
  SET_CIRCUIT_COUNTRY,
  SET_CIRCUIT_NAME,
  SET_YEAR,
  SET_CONSTRUCTOR_NAME,
  SET_CONSTRUCTOR_COUNTRY,
  SET_DRIVER_SURNAME,
  SET_DRIVER_NATIONALITY,
} from '../action-types';

export interface FiltersState {
  year?: number;
  circuitName: string;
  circuitCountry: string;
  constructorName: string;
  constructorCountry: string;
  driverSurname: string;
  driverNationality: string;
}

export interface FiltersAction extends Action<string> {
  payload: Partial<FiltersState>;
}

const initialState: FiltersState = {
  year: undefined,
  circuitName: '',
  circuitCountry: '',
  constructorName: '',
  constructorCountry: '',
  driverSurname: '',
  driverNationality: '',
};

const handleSetYear = (state: FiltersState, action: FiltersAction) => ({
  ...state,
  year: action.payload.year,
});

const handleSetCircuitName = (state: FiltersState, action: FiltersAction) => ({
  ...state,
  circuitName: action.payload.circuitName,
});

const handleSetCircuitCountry = (
  state: FiltersState,
  action: FiltersAction
) => ({
  ...state,
  circuitCountry: action.payload.circuitCountry,
});

const handleSetConstructorName = (
  state: FiltersState,
  action: FiltersAction
) => ({
  ...state,
  constructorName: action.payload.constructorName,
});

const handleSetConstructorCountry = (
  state: FiltersState,
  action: FiltersAction
) => ({
  ...state,
  constructorCountry: action.payload.constructorCountry,
});

const handleSetDriverSurname = (
  state: FiltersState,
  action: FiltersAction
) => ({
  ...state,
  driverSurname: action.payload.driverSurname,
});

const handleSetDriverNationality = (
  state: FiltersState,
  action: FiltersAction
) => ({
  ...state,
  driverNationality: action.payload.driverNationality,
});

const handlers = {
  [SET_YEAR]: handleSetYear,
  [SET_CIRCUIT_NAME]: handleSetCircuitName,
  [SET_CIRCUIT_COUNTRY]: handleSetCircuitCountry,
  [SET_CONSTRUCTOR_NAME]: handleSetConstructorName,
  [SET_CONSTRUCTOR_COUNTRY]: handleSetConstructorCountry,
  [SET_DRIVER_SURNAME]: handleSetDriverSurname,
  [SET_DRIVER_NATIONALITY]: handleSetDriverNationality,
};

export default function filters(
  state = initialState,
  action: FiltersAction
): FiltersState {
  const handler = handlers[action.type];
  console.log(action, handler);
  return handler ? handler(state, action) : state;
}
