export type Link = {
  href: string;
  label: string;
};

export type NavigationRoute = {
  name: string;
  path: string;
  Component?: React.ComponentType;
};

export type Circuit = {
  circuitId: string;
  url: string;
  circuitName: string;
  Location: {
    lat: string;
    long: string;
    locality: string;
    country: string;
  };
};

export type Constructor = {
  constructorId: string;
  url: string;
  name: string;
  nationality: string;
};

export type Drivers = {
  driverId: string;
  url: string;
  givenName: string;
  familyName: string;
  nationality: string;
};
