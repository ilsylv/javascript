import { getNavigations } from '@ijl/cli';

const navigations = getNavigations('javascript');

export const baseUrl = navigations['javascript'];

export const URLs = {
  root: {
    url: navigations['javascript.app'],
  },
  constructors: {
    url: navigations['link.javascript.app.constructors'],
  },
  drivers: {
    url: navigations['link.javascript.app.drivers'],
  },
  circuits: {
    url: navigations['link.javascript.app.circuits'],
  },
  season: {
    url: navigations['link.javascript.app.season'],
  },
};
