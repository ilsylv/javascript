import React from 'react';
import './root.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Navigation } from './components/navigation';
import Dashboard from './containers/dashboard';
import { Provider } from 'react-redux';
import { store } from './__data__/store';

const App = () => (
  <Provider store={store}>
    <Router>
      <Navigation />
      <Dashboard />
    </Router>
  </Provider>
);

export default App;
