import React, { useState } from 'react';
import { NavigationRoute } from '../../__data__/model';
import { Link } from 'react-router-dom';
import ListConstructors from '../../components/constructors/list';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Filter from '../../components/constructors/filter';

import style from '../../components/circuits/style.css';

export interface IListConstructorsProps {}

export default function Constructors() {
  return (
    <>
      <Grid container direction="row" justify="center" alignItems="center">
        <Paper>
          <Typography variant="h4" gutterBottom>
            Конструкторы
          </Typography>

          <Paper>
            <Filter />
          </Paper>

          <Paper>
            <ListConstructors />
          </Paper>
        </Paper>
      </Grid>
    </>
  );
}
