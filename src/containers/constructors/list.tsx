import React, { useMemo, useState } from 'react';
import ApiWrapper from '../../api/index';
import Constructors from './constructors';
import style from '../../components/circuits/style.css';
import { cn as classNames } from 'classnames';
import { Constructor } from '../../__data__/model';
import { Paper, TableContainer } from '@material-ui/core';
import { Link } from 'react-router-dom';

import {
  withStyles,
  Theme,
  createStyles,
  makeStyles,
} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

export interface IListConstructorsProps {}

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function ListConstructors(props: IListConstructorsProps) {
  const [constructors, setConstructors] = useState([]);

  React.useEffect(() => {
    ApiWrapper.fetch<Constructor, 'ConstructorTable', 'Constructors'>(
      'constructors'
    )
      .then((res) => setConstructors(res.MRData.ConstructorTable.Constructors))
      .catch((e) => {
        console.error(e);
      });
  }, []);

  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <TableCell>Назвние команды</TableCell>
            <TableCell align="right">Страна</TableCell>
            <TableCell align="right">Ссылка на википедию</TableCell>
            {/*<TableCell align="right">Carbs&nbsp;(g)</TableCell>*/}
            {/*<TableCell align="right">Protein&nbsp;(g)</TableCell>*/}
          </TableRow>
        </TableHead>
        <TableBody>
          {constructors.map((item) => (
            <TableRow key={item.name}>
              <TableCell component="th" scope="row">
                {item.name}
              </TableCell>
              <TableCell align="right">{item.nationality}</TableCell>
              <TableCell align="right">
                <a href={item.url} target="_blank" rel="noreferrer">
                  Посмотреть подробнее{' '}
                </a>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
