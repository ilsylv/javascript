import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { URLs } from '../__data__/urls';
import LazyComponent from '../components/lazy-component';

import Constructors from './constructors/constructors';
import Main from './main';
import Circuits from '../components/circuits/circuits';
import Drivers from '../components/drivers/drivers';

const Dashboard = () => (
  <Switch>
    <Route path={URLs.constructors.url}>
      <LazyComponent>
        <Constructors />
      </LazyComponent>
    </Route>
    <Route path={URLs.drivers.url}>
      <LazyComponent>
        <Drivers />
      </LazyComponent>
    </Route>
    <Route path={URLs.circuits.url}>
      <LazyComponent>
        <Circuits />
      </LazyComponent>
    </Route>
    {/*<Route path={URLs.season.url}>*/}
    {/*    <LazyComponent>*/}
    {/*        <Season />*/}
    {/*    </LazyComponent>*/}
    {/*</Route>*/}
    <Route path={URLs.root.url}>
      <LazyComponent>
        <Main />
      </LazyComponent>
    </Route>

    <Route path="*">
      <h1>Not Found</h1>
    </Route>
  </Switch>
);

export default Dashboard;
