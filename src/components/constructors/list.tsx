import React, { useState } from 'react';
import ApiWrapper from '../../api';
import { Paper, TableContainer } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { RootState } from '../../__data__/store/reducers';
import { Constructor } from '../../__data__/model';

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function ListConstructors() {
  const year = useSelector<RootState, number>((state) => state.filters.year);
  const constructorName = useSelector<RootState, string>(
    (state) => state.filters.constructorName
  );
  const constructorCountry = useSelector<RootState, string>(
    (state) => state.filters.constructorCountry
  );

  const [constructors, setConstructors] = useState<Constructor[]>([]);
  const [filterConstructors, setFilterConstructors] = useState<Constructor[]>(
    []
  );

  React.useEffect(() => {
    ApiWrapper.fetch<Constructor, 'ConstructorTable', 'Constructors'>(
      'constructors',
      year
    )
      .then((res) => {
        setConstructors(res.MRData.ConstructorTable.Constructors);
        setFilterConstructors(res.MRData.ConstructorTable.Constructors);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [year]);

  React.useEffect(() => {
    setFilterConstructors(
      constructors
        .filter((constructor) => {
          return constructor.name
            .toLowerCase()
            .includes(constructorName.toLowerCase());
        })
        .filter((constructor) => {
          return constructor.nationality
            .toLowerCase()
            .includes(constructorCountry.toLowerCase());
        })
    );
  }, [constructorName, constructorCountry]);

  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <TableCell>Назвние команды</TableCell>
            <TableCell align="right">Страна</TableCell>
            <TableCell align="right">Ссылка на википедию</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {filterConstructors.map((item) => (
            <TableRow key={item.name}>
              <TableCell component="th" scope="row">
                {item.name}
              </TableCell>
              <TableCell align="right">{item.nationality}</TableCell>
              <TableCell align="right">
                <a href={item.url} target="_blank" rel="noreferrer">
                  Посмотреть подробнее
                </a>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
