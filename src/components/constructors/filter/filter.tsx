import React from 'react';
import _ from 'lodash';
import dayjs from 'dayjs';

import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../__data__/store/reducers';
import {
  setFiltersConstructorName,
  setFiltersConstructorCountry,
  setFiltersYear,
} from '../../../__data__/store/actions/filters';

import {
  Select,
  FormControl,
  MenuItem,
  InputLabel,
  Paper,
  TextField,
  Grid,
} from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

interface Filter {
  valueOne: string;
  valueTwo: string;
  valueThree: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  })
);

export default function Filter() {
  const dispatch = useDispatch();

  const classes = useStyles();

  const year = useSelector<RootState, number>((state) => state.filters.year);
  const constructorName = useSelector<RootState, string>(
    (state) => state.filters.constructorName
  );
  const constructorCountry = useSelector<RootState, string>(
    (state) => state.filters.constructorCountry
  );

  const years = _.range(1950, dayjs().year() + 1).map((item) => ({
    label: item,
    value: item,
  }));

  const onYearChange = (event) => {
    dispatch(setFiltersYear(event.target.value));
    console.log(event);
  };

  const onConstructorsName = (value) => {
    dispatch(setFiltersConstructorName(value));
  };

  const onConstructorsCountry = (value) => {
    dispatch(setFiltersConstructorCountry(value));
  };

  return (
    <Paper>
      <Grid direction="column">
        <div>
          <h3>Фильтр</h3>
        </div>

        <Grid direction="row" justify="space-between" alignItems="center">
          <FormControl className={classes.formControl}>
            <TextField
              id="team-name"
              value={constructorName}
              onChange={(event) =>
                onConstructorsName(event.currentTarget.value)
              }
              label="Название команды"
            />
          </FormControl>

          <FormControl className={classes.formControl}>
            <TextField
              id="team-country"
              value={constructorCountry}
              onChange={(event) =>
                onConstructorsCountry(event.currentTarget.value)
              }
              label="Страна"
            />
          </FormControl>

          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="native-simple">СЕЗОН</InputLabel>

            <Select
              id="select-year"
              inputProps={{
                id: 'native-simple',
              }}
              value={year}
              onChange={onYearChange}
            >
              <MenuItem value={undefined}> сбросить года </MenuItem>
              {years.map((item) => (
                <MenuItem key={item.value} value={item.value}>
                  {' '}
                  {item.label}{' '}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    </Paper>
  );
}
