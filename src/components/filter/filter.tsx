import React from 'react';
import _ from 'lodash';
import dayjs from 'dayjs';

import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../__data__/store/reducers';
import {
  setFiltersCircuitCountry,
  setFiltersCircuitName,
  setFiltersYear,
} from '../../__data__/store/actions/filters';

import {
  Select,
  FormControl,
  MenuItem,
  InputLabel,
  Paper,
  TextField,
  Grid,
} from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

interface Filter {
  valueOne: string;
  valueTwo: string;
  valueThree: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  })
);

export default function Filter() {
  const dispatch = useDispatch();

  const classes = useStyles();

  const year = useSelector<RootState, number>((state) => state.filters.year);
  const circuitName = useSelector<RootState, string>(
    (state) => state.filters.circuitName
  );
  const circuitCountry = useSelector<RootState, string>(
    (state) => state.filters.circuitCountry
  );

  const years = _.range(1950, dayjs().year() + 1).map((item) => ({
    label: item,
    value: item,
  }));

  const onYearChange = (event) => {
    dispatch(setFiltersYear(event.target.value));
    console.log(event);
  };

  const onCircuitsName = (value) => {
    dispatch(setFiltersCircuitName(value));
  };

  const onCircuitsCountry = (value) => {
    dispatch(setFiltersCircuitCountry(value));
  };

  return (
    <Paper>
      <Grid container direction="column">
        <div>
          <h3>Фильтр</h3>
        </div>

        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          <FormControl className={classes.formControl}>
            <TextField
              id="circuits-name"
              value={circuitName}
              onChange={(event) => onCircuitsName(event.currentTarget.value)}
              label="Название трассы"
            />
          </FormControl>

          <FormControl className={classes.formControl}>
            <TextField
              id="circuits-country"
              value={circuitCountry}
              onChange={(event) => onCircuitsCountry(event.currentTarget.value)}
              label="Страна"
            />
          </FormControl>

          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="native-simple">СЕЗОН</InputLabel>

            <Select
              id="select-year"
              inputProps={{
                id: 'native-simple',
              }}
              value={year}
              onChange={onYearChange}
            >
              <MenuItem value={undefined}> сбросить года </MenuItem>
              {years.map((item) => (
                <MenuItem key={item.value} value={item.value}>
                  {' '}
                  {item.label}{' '}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    </Paper>
  );
}
