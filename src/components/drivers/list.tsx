import React, { useState } from 'react';
import ApiWrapper from '../../api';
import { Drivers } from '../../__data__/model';
import { Paper, TableContainer } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

import { RootState } from '../../__data__/store/reducers';

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function ListDrivers() {
  const year = useSelector<RootState, number>((state) => state.filters.year);
  const driverSurname = useSelector<RootState, string>(
    (state) => state.filters.driverSurname
  );
  const driverNationality = useSelector<RootState, string>(
    (state) => state.filters.driverNationality
  );

  const [drivers, setDrivers] = useState<Drivers[]>([]);
  const [filterDrivers, setFilterDrivers] = useState<Drivers[]>([]);

  React.useEffect(() => {
    ApiWrapper.fetch<Drivers, 'DriverTable', 'Drivers'>('drivers', year)
      .then((res) => {
        setDrivers(res.MRData.DriverTable.Drivers);
        setFilterDrivers(res.MRData.DriverTable.Drivers);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [year]);
  console.log(filterDrivers);
  React.useEffect(() => {
    setFilterDrivers(
      drivers
        .filter((driver) => {
          return driver.familyName
            .toLowerCase()
            .includes(driverSurname.toLowerCase());
        })
        .filter((driver) => {
          return driver.nationality
            .toLowerCase()
            .includes(driverNationality.toLowerCase());
        })
    );
  }, [driverSurname, driverNationality]);

  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <TableCell>Фамилия пилота</TableCell>
            <TableCell align="right">Имя пилота</TableCell>
            <TableCell align="right">Национальность</TableCell>
            <TableCell align="right">Ссылка на википедию</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {filterDrivers.map((item, idx) => (
            <TableRow key={idx}>
              <TableCell component="th" scope="row">
                {item.familyName}
              </TableCell>
              <TableCell align="right">{item.givenName}</TableCell>
              <TableCell align="right">{item.nationality}</TableCell>
              <TableCell align="right">
                <a href={item.url} target="_blank" rel="noreferrer">
                  Посмотреть подробнее
                </a>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
