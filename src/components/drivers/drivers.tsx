import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Filter from './filter/filter';
import ListDrivers from '../drivers/list';

// import ListDrivers from
// import Filter from '../filter/index';

export default function Drivers() {
  return (
    <>
      <Grid container direction="row" justify="center" alignItems="center">
        <Paper>
          <Typography variant="h4" gutterBottom>
            Пилоты
          </Typography>
          <Filter />
          <Paper>
            <ListDrivers />
          </Paper>
        </Paper>
      </Grid>
    </>
  );
}
