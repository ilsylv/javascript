import React from 'react';

import style from './style.css';

type ErrprPops = {
  error?: {
    text: string;
    title?: string;
  };
};

const Error: React.FC<ErrprPops> = ({ error: { text, title } }) => (
  <div className={style.wrapper}>
    {title && <h2 className={style.title}>{title}</h2>}
    <p className={style.errorText}>{text}</p>
  </div>
);

Error.defaultProps = {
  error: {
    text: 'Извините, что-то пошло не так',
  },
};

export default Error;
