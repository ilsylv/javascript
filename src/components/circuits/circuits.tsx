import React from 'react';
import ListCircuits from './list';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Filter from '../filter/index';

export default function Circuits() {
  return (
    <>
      <Grid container direction="row" justify="center" alignItems="center">
        <Paper>
          <Typography variant="h4" gutterBottom>
            Трассы
          </Typography>
          <Filter />
          <Paper>
            <ListCircuits />
          </Paper>
        </Paper>
      </Grid>
    </>
  );
}

//only snapshot
