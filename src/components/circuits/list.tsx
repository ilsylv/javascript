import React, { useState } from 'react';
import ApiWrapper from '../../api';
import Circuits from './circuits';
import { Circuit } from '../../__data__/model';
import { Paper, TableContainer } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { RootState } from '../../__data__/store/reducers';

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

//only shanpshot
export default function ListCircuits() {
  const year = useSelector<RootState, number>((state) => state.filters.year);
  const circuitName = useSelector<RootState, string>(
    (state) => state.filters.circuitName
  );
  const circuitCountry = useSelector<RootState, string>(
    (state) => state.filters.circuitCountry
  );

  const [circuits, setCircuits] = useState<Circuit[]>([]);
  const [filterCircuits, setFilterCircuits] = useState<Circuit[]>([]);

  React.useEffect(() => {
    ApiWrapper.fetch<Circuit, 'CircuitTable', 'Circuits'>('circuits', year)
      .then((res) => {
        setCircuits(res.MRData.CircuitTable.Circuits);
        setFilterCircuits(res.MRData.CircuitTable.Circuits);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [year]);

  React.useEffect(() => {
    setFilterCircuits(
      circuits
        .filter((circuit) => {
          return circuit.circuitName
            .toLowerCase()
            .includes(circuitName.toLowerCase());
        })
        .filter((circuit) => {
          return circuit.Location.country
            .toLowerCase()
            .includes(circuitCountry.toLowerCase());
        })
    );
  }, [circuitName, circuitCountry]);

  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <TableCell>Назвние трассы</TableCell>
            <TableCell align="right">Страна</TableCell>
            <TableCell align="right">Местонахождение</TableCell>
            <TableCell align="right">Ссылка на википедию</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {filterCircuits.map((item) => (
            <TableRow key={item.circuitName}>
              <TableCell component="th" scope="row">
                {item.circuitName}
              </TableCell>
              <TableCell align="right">{item.Location.country}</TableCell>
              <TableCell align="right">{item.Location.locality}</TableCell>
              <TableCell align="right">
                <a href={item.url} target="_blank" rel="noreferrer">
                  Посмотреть подробнее
                </a>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
