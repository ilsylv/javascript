import Drivers from '../drivers/drivers';
import ListDrivers from '../drivers/list';
import Filter from '../drivers/filter';

import { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';

import { describe, it, expect, beforeEach } from '@jest/globals';

describe('Тестируем компоненты страниц Водителей', () => {
  it('тестируем компонент ListDrivers', () => {
    const wrapperListDrivers = mount(
      <Provider store={store}>
        {' '}
        <ListDrivers />
      </Provider>
    );
    expect(wrapperListDrivers).toMatchSnapshot();
  });

  it('тестируем компонент Drivers', () => {
    const wrapperDrivers = mount(
      <Provider store={store}>
        {' '}
        <Drivers />{' '}
      </Provider>
    );
    expect(wrapperDrivers).toMatchSnapshot();
  });

  it('тестируем компонент Filter', () => {
    const component = mount(
      <Provider store={store}>
        <Filter />
      </Provider>
    );

    expect(component).toMatchSnapshot();

    component.find('input#surname-driver').simulate('change', {
      target: {
        value: 'test',
      },
    });

    component.find('input#nationality-driver').simulate('change', {
      target: {
        value: 'test',
      },
    });

    expect(component.find('div#select-year').exists()).toBeTruthy();
    expect(component.find('ul[role="listbox"]').exists()).toBeFalsy();

    component.update();
    expect(component).toMatchSnapshot();
  });
});
