import ListCircuits from '../circuits/list';
import Circuits from '../circuits/circuits';
import Filter from '../filter/filter';

import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';

import { describe, it, expect, beforeEach } from '@jest/globals';

describe('Тестируем компоненты страниц Трассы', () => {
  it('тестируем компонент ListCircuits', () => {
    const wrapperListCircuits = mount(
      <Provider store={store}>
        {' '}
        <ListCircuits />
      </Provider>
    );
    expect(wrapperListCircuits).toMatchSnapshot();
  });

  it('тестируем компонент Circuits', () => {
    const wrapperCircuits = mount(
      <Provider store={store}>
        {' '}
        <Circuits />{' '}
      </Provider>
    );
    expect(wrapperCircuits).toMatchSnapshot();
  });

  it('тестируем компонент Filter', () => {
    const component = mount(
      <Provider store={store}>
        <Filter />
      </Provider>
    );

    expect(component).toMatchSnapshot();

    component.find('input#circuits-name').simulate('change', {
      target: {
        value: 'test',
      },
    });

    component.find('input#circuits-country').simulate('change', {
      target: {
        value: 'test',
      },
    });

    expect(component.find('div#select-year').exists()).toBeTruthy();
    expect(component.find('ul[role="listbox"]').exists()).toBeFalsy();

    component.update();
    expect(component).toMatchSnapshot();
  });
});
