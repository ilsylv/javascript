import Constructors from '../../containers/constructors';
import Filter from '../filter/filter';
import ListConstructors from '../constructors/list';

import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';

import { describe, it, expect, beforeEach } from '@jest/globals';

describe('Тестируем компоненты страниц Команды', () => {
  it('тестируем компонент ListConstructors', () => {
    const wrapperListConstructors = mount(
      <Provider store={store}>
        {' '}
        <ListConstructors />
      </Provider>
    );
    expect(wrapperListConstructors).toMatchSnapshot();
  });

  it('тестируем компонент Constructors', () => {
    const wrapperConstructors = mount(
      <Provider store={store}>
        {' '}
        <Constructors />{' '}
      </Provider>
    );
    expect(wrapperConstructors).toMatchSnapshot();
  });

  it('тестируем компонент Filter', () => {
    const component = mount(
      <Provider store={store}>
        <Filter />
      </Provider>
    );

    expect(component).toMatchSnapshot();

    // component.find('input#team-name').simulate('change', {
    //     target: {
    //         value: 'test'
    //     }})

    // component.find('input#team-country').simulate('change', {
    //     target: {
    //         value: 'test'
    //     }})

    expect(component.find('div#select-year').exists()).toBeTruthy();
    expect(component.find('ul[role="listbox"]').exists()).toBeFalsy();

    component.update();
    expect(component).toMatchSnapshot();
  });
});
