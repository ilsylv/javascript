import React from 'react';

import Error from '../error';

class ErrorBoundary extends React.Component {
  state = {
    isError: false,
  };

  static getDerivedStateFromError() {
    return {
      isError: true,
    };
  }

  render() {
    const { isError } = this.state;

    if (isError) {
      return <Error />;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
