import React from 'react';
import { AppBar, Button, Toolbar, Typography } from '@material-ui/core';
import { URLs } from '../../__data__/urls';
import { Link } from 'react-router-dom';

export const Navigation = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <Link to={URLs.root.url}>
          <Typography variant="h6">Formula 1 (главная страница)</Typography>
        </Link>
        <Link to={URLs.circuits.url}>
          <Button color="inherit">Трассы</Button>
        </Link>

        <Link to={URLs.drivers.url}>
          <Button color="inherit">Водители</Button>
        </Link>

        <Link to={URLs.constructors.url}>
          <Button color="inherit">Конструкторы</Button>
        </Link>

        <Button color="inherit">Сезоны</Button>
      </Toolbar>
    </AppBar>
  );
};
