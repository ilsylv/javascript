import axios from 'axios';

const BASE = 'http://ergast.com/api/f1/';

interface ApiWrapperResponce<
  T extends Record<string, unknown>,
  K1 extends string,
  K2 extends string
> {
  MRData: {
    [key in K1]: {
      [key in K2]: T[];
    };
  };
}

class ApiWrapper {
  static async fetch<
    T extends Record<string, unknown>,
    K1 extends string,
    K2 extends string
  >(method?: string, season: string | number = '') {
    const response = await axios.get(`${BASE}${season}/${method}.json`);
    const { MRData } = response.data;

    const { data } = await axios.get<ApiWrapperResponce<T, K1, K2>>(
      `${BASE}${season}/${method}.json?limit=${MRData.total}`
    );

    return data;
  }
}

export default ApiWrapper;
