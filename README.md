# Сервис предоставляющий информацию об участников F1.

## MVP 1

- Главная страница со списоком соревнований в разные года, с фильтрацией.
- Страница со списоком участников.
- Статистика побед участников.

## Установка

```bash
npm install
```

## Запуск

```bash
npm start
```

[Figma](https://www.figma.com/file/nA6PQLh6ymlKzZ7NgGsJi7/Untitled)
